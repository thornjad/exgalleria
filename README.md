# ExGalleria -- A minimal native lightbox library

- Small
- Simple and minimal
- Images, text and ajax content
- Namespaced for minimal intrusion
- Responsive
- Accessible
- Customizable
- Native, supporting the latest modern browsers without relying on jQuery

## Installation

### CDN

An optimized release of ExGalleria is available via unpkg. CSS should be loaded first in the `<head>`:

```html
<link href="https://unpkg.com/thornjad/exgalleria/lib/exgalleria.css" type="text/css" rel="stylesheet" />
```

Then the JS library. It is strongly recommended to include this library at the bottom of the page, just before the closing `</body>` tag.

```html
<script async src="https://unpkg.com/thornjad/exgalleria/lib/exgalleria.js"></script>
```

Note the `async` _can_ be excluded if you know what you're doing, but using `async` or at least `defer` is strongly recommended.

### npm

ExGalleria can also be loaded as a dependency via npm. This is especially useful if you'd like to serve all content from your own domain rather than relying on an external CDN, even though unpkg is very reputable.

```sh
npm install --save exgalleria
```

## Usage

ExGalleria is a minimal library and depends on a semi-rigid layout with a small set of configuration options. Here is the layout:

- A non-self-closing element (`<section>` is recommended) with the class `exgalleria-gallery`. This is the _container element_.
- Within the _container element_, there must be one or more `<a>` elements. Zero elements will put a warning in the console and the gallery will not display. These are the _child elements_.
- The `<a>`'s `href` attribute must be either a URL for the image to show, or empty or `"#"`
  - If `href` is blank or `"#"` and there is HTML within the `<a>`, that HTML will be shown in the gallery
  - If `href` is blank or `"#"` and there is a non-blank `data-exgalleria-ajax` attribute, the Ajax content will be shown in the gallery (See the [attributes reference](#attribute-reference) below)

Example:

```html
<section class="exgalleria-gallery">
  <a href="/path/to/image.png">Click to see image</a>
  <a href="/path/to/image2.png"><img src="thumbnail.jpg" /></a>
  <a href="">Text displayed in the gallery</a>
  <a href="" data-exgalleria-ajax="/otherFile.html .selector">Show content from another file</a>
</section>
```

Note that swipe events on mobile are not currently supported.

### Attribute Reference

#### `data-exgalleria-ajax`

Used on the _child element_

If this attribute is present on a _child element_ *and* is not blank *and* the `href` attribute is either blank or `"#"`, then the content of `data-exgalleria-ajax` will be used to asynchronously load an external file, or part of a file, into the gallery. The presence of a non-blank, non-`"#"` `href` will override this attribute.

If the file specified by the URL can be determined to be an image file, ExGalleria will act as if the URL were in the `href` attribute. Otherwise, it will attempt to load the entire file into the gallery.

If there is a URL *and* a DOM query provided (e.g. `/foo.html .image`), ExGalleria will load the URL as HTML and attempt to put the specified element into the gallery.

#### `data-exgalleria-root`

Used on the _container element_

A query string specifying where the gallery should be appended. Default is `"body"`.

#### `data-exgalleria-speed`

Used on the _container element_

Customize the speed of fade animation between images. Default is 150.

#### `data-exgalleria-type`

Used on the _child elements_

ExGalleria makes an educated guess about the type of content to load into the gallery. This attribute can be used on `<a>` elements to override this guessing and force a content type. Currently supported types are:

- "image"
- "html"
- "text"

## Example

```html
<section class="exgalleria-gallery" data-exgalleria-speed="100" data-exgalleria-root="body > .myGalleryContainer">
  <a href="/path/to/foo.png" data-exgalleria-type="image"><img src="/this/is/the/thumbnail.jpg" /></a>
  <a href="" data-exgalleria-ajax="/other/file.html #gallery-image">Click for the image</a>
  <a href="">This text is displayed in the gallery</a>
</section>
```

## Contributing

Contributions are welcome! To report a bug, open an issue on (GitLab)[https://gitlab.com/thornjad/exgalleria/issues]. Be sure to include as much information as possible, including version numbers and browser(s) where the issue occurs.

Merge requests *must* be made against `master` and *must* be made from your own fork in a branch *other than `master`*. Please ensure the tests succeed with `npm test` before opening your request.

### License

Copyright (c) 2019 Jade Michael Thornton under the terms of the MIT license. See <LICENSE> for more information.
